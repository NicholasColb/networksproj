var mongo = require('mongodb');
var cors = require('cors');
require('dotenv').config();

const express = require('express');
const app = express();
const port = 8080;
var bodyParser = require('body-parser');
var path    = require("path");

var uri = process.env.DB_CONN;
var MongoClient = require('mongodb').MongoClient;
var db;
 MongoClient.connect(uri, { useNewUrlParser: true }, function(err, client) {
   // perform actions on the collection object
   if(err) {
     console.log(err);
   } else {
     console.log("connected!");
     db =  client.db("iv16");
   }

});

function compare(a, b) {
  if(a.rank < b.rank)
    return 1;
  if(a.rank > b.rank)
    return -1;
  return 0;
}

const fakeNames = {
  'Markus Ihamuotila': 'chicken',
  'Onni Paloheimo': 'lamb',
  'Roosa Kujanpää': 'puppy',
  'Joonas Rasa': 'dog',
  'Kasper Nurminen': 'cat',
  'Katariina Korolainen': 'rabbit',
  'Atte Mäkinen': 'eagle',
  'Ella Huttunen': 'sparrow',
  'Aleksi Löytynoja': 'hen',
  'Samuel Moawad': 'dove',
  'Sini Hölsä': 'pigeon',
  'Elena Rima': 'mouse',
  'Arttu Engström': 'kangaroo',
  'Antti Kari-Koskinen': 'elephant',
  'Ella Palo': 'zebra',
  'Juhana Saarinen': 'salmon',
  'Aura Kiiskinen': 'frog',
  'Anni Kivilaakso': 'spider',
  'Satu Reijonen': 'ladybug',
  'Riikka Eskola': 'bear',
  'Eino Vuorinen': 'tiger',
  'Sanni Lares': 'lion',
  'Aili Hukka': 'cheetah',
  'Emma Kankkunen': 'ostrich',
  'Liva Westerback': 'giraffe',
  'Julia Korhonen': 'alligator',
  'Marika Sillanpää': 'badger',
  'Ella Anttila': 'crow',
  'Inkeri Rouvinen': 'fox',
  'Max Naumanen': 'bobcat',
  'Ville Pelkonen': 'lynx',
  'Alpo Peltola': 'pig',
}



app.use( bodyParser.json() );
app.use(cors());

app.get('/indexes', function(req, res) {
  db.collection("relations").find({}).toArray(function(err, result){
    if(err) throw err;
    res.send(result);
  });

});

app.get('/login', function(req, res) {
  console.log('logged in');
  res.send('Authorized');
});

app.post('/insert', function(req, res) {

    req.body.name = fakeNames[req.body.name];
    req.body.relations.sort(compare);
    req.body.relations.map(e => {
      e.name = fakeNames[e.name];
    });

    db.collection("relations").update({name: req.body.name}, req.body, {upsert: true}, function(err, result){
          if(err) {
            console.log(err);
          } else {
            res.send(result);
          }

        });
});
app.use(express.static(__dirname + '/public'));

app.get('*', function(req, res){
  res.sendfile(__dirname + '/public/index.html');
});
app.listen(port, ()=> console.log(`app listening on port ${port}!`));
